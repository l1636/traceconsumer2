import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.function.Consumer;
…
	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
	private  TraceRepository tr;
       @Autowired
	private  TransportationOrderRepository orders;

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			tr.save(t);
                    TransportationOrder result = orders.findById(t.getTruck()).orElse(new TransportationOrder());
			if (result != null && result.getTruck()!= null
                        && ! result.getTruck().equals("")
                        && result.getSt() == 0) {
				result.setLastDate(t.getLastSeen());
				result.setLastLat(t.getLat());
				result.setLastLong(t.getLng());
				if (result.distanceToDestination() < 10)
					result.setSt(1);
				orders.save(result);
				log.info("Order updated: "+ result);
			}
		};
	}
}

